**It is an online iterview feedback application that helps you to keep track of interview feedbacks**

**Demo** - https://vimeo.com/311876678


**Tools and Technologies used :**

    - React JS
    - ES6
    - JavaScript
    - CSS
	

**Local Setup -**

  1. Clone the repository.
  3. Run command "npm install" inside project root directory to install dependencies.
  4. Run command "npm start" to run the application.

**NOTE** - Change the API call base url in ```src/api-calls/url.js``` to ```http://localhost:<port>/```

For any help, drop a mail to **varshant.14@gmail.com**