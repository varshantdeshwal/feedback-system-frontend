import React, { Component } from "react";
import ReactLoading from "react-loading";

class Spinner extends Component {
  state = {};
  render() {
    return (
      <div className="spinner-container">
        <div className="spinner">
          <ReactLoading
            type="spokes"
            color="rgb(43, 73, 136)"
            height={"100%"}
            width={"100%"}
          />
        </div>
      </div>
    );
  }
}

export default Spinner;
