import React, { Component } from "react";

import FeedbackFields from "./FeedbackFields";
import Skills from "./Skills";
import { Redirect } from "react-router-dom";
import addFeedback from "../api-calls/feedback/addFeedback";
import uploadResume from "../api-calls/feedback/uploadResume";
import fetchOptions from "../api-calls/common/fetchOptions";
import Spinner from "./Spinner";
import editFeedback from "../api-calls/feedback/editFeedback";
import deleteFeedbackRequest from "../api-calls/feedbackRequest/deleteFeedbackRequest";
import getFeedback from "../api-calls/feedback/getFeedback";
class Feedback extends Component {
  state = {
    positions: [],
    skillsToBeRated: [],
    availableSkills: [],
    status: "",
    ratings: [],
    comments: "",
    spinner: false,
    oldFeedback: "",
    migrateToFeedbacks: false,
    years: "",
    months: ""
  };
  componentDidMount() {
    fetchOptions("skills", this.props.token).then(res => {
      if (res.message === "success") {
        let skillsArray = [];
        res.doc.map(skill => {
          skillsArray.push({ value: skill.name, label: skill.name });
        });
        this.setState({ availableSkills: skillsArray });
      }
    });
    fetchOptions("designations", this.props.token).then(res => {
      if (res.message === "success") {
        let positionsArray = [];
        res.doc.map(skill => {
          positionsArray.push(skill.name);
        });
        this.setState({ positions: positionsArray });
      }
    });
    this.setState({
      migrateToFeedbacks: false
    });
    if (this.props.requestId != null) {
      getFeedback(this.props.oldInterviewId, this.props.token).then(res => {
        if (res.message === "successful") {
          this.setState({
            oldFeedback: res.doc
          });
        }
      });
    }
  }

  captureDate = date => {
    this.setState({ date: date });
  };
  captureChange = (e, name) => {
    this.setState({ [name]: e.target.value });
  };
  captureSkills = arr => {
    let ratingsCopy = this.state.ratings;

    let arrLabels = [];

    if (this.state.skillsToBeRated.length < arr.length) {
      ratingsCopy.push({ name: arr[arr.length - 1].value, rating: 0 });
    } else if (this.state.skillsToBeRated.length > arr.length) {
      arr.map(item => {
        arrLabels.push(item.label);
      });

      ratingsCopy = ratingsCopy.filter(data => {
        return arrLabels.includes(data.name);
      });
    } else if (arr.length === 0) {
      ratingsCopy = [];
    }

    this.setState({ skillsToBeRated: arr, ratings: ratingsCopy });
  };
  onStarClick = (nextValue, prevValue, name) => {
    let ratingsCopy = this.state.ratings;

    for (let i = 0; i < ratingsCopy.length; i++) {
      if (ratingsCopy[i].name === name) {
        ratingsCopy[i].rating = nextValue;
        break;
      }
    }

    this.setState({ ratings: ratingsCopy });
    // console.log(name, nextValue);
  };

  submitFeedback = e => {
    e.preventDefault();
    this.setState({ spinner: true });

    if (this.props.requestId != null) {
      let feedback = {
        interviewerId: this.props.interviewerId,

        date: this.state.date,

        skills: this.state.ratings,
        comments: this.state.comments,
        status: this.state.status
      };
      let details = JSON.stringify(feedback);
      editFeedback(this.props.oldInterviewId, details, this.props.token).then(
        res => {
          if (res.message === "successful") {
            deleteFeedbackRequest(this.props.requestId, this.props.token).then(
              resp => {
                if (resp.message === "deleted") this.props.search();
                this.setState({ spinner: false, migrateToFeedbacks: true });
                if (this.props.type === "admin") this.props.refresh();
                this.props.handleNotification("Feedback saved");
              }
            );
          }
        }
      );
    } else {
      let years = this.state.years === "" ? 0 : e.target[2].value;
      let months = this.state.months === "" ? 0 : e.target[3].value;

      let feedback = {
        interviewerId: this.props.interviewerId,
        applicantName: e.target[0].value,
        date: this.state.date,
        workExperience: {
          years: years,
          months: months
        },
        applyingFor: e.target[4].value,
        skills: this.state.ratings,
        comments: this.state.comments,
        status: this.state.status
      };
      addFeedback(JSON.stringify(feedback), this.props.token).then(res => {
        let formData = new FormData();
        formData.append("interviewID", res.doc._id + ".pdf");
        formData.append("resume", this.state.file);
        this.props.search();
        this.setState({ spinner: false, migrateToFeedbacks: true });
        uploadResume(formData, this.props.token).then(res => {
          this.props.handleNotification("Feedback saved");
        });
      });
    }
  };
  captureFile = e => {
    let file = e.target.files[0];

    this.setState({ file: file });
  };
  render() {
    console.log("state", this.state);
    return (
      <div>
        {this.state.spinner ? (
          <Spinner />
        ) : (
          <div className="container border feedback-form">
            <h1 align="center">By {this.props.interviewerName}</h1>
            <FeedbackFields
              handleDate={this.captureDate}
              date={this.state.date}
              handleChange={(e, name) => {
                this.captureChange(e, name);
              }}
              positions={this.state.positions}
              captureSkills={this.captureSkills}
              availableSkills={this.state.availableSkills}
              onStarClick={this.onStarClick}
              skillsToBeRated={this.state.skillsToBeRated}
              status={this.state.status}
              submitFeedback={this.submitFeedback}
              ratings={this.state.ratings}
              handleFile={this.captureFile}
              oldFeedback={this.state.oldFeedback}
            />
          </div>
        )}
        {this.state.migrateToFeedbacks ? (
          <Redirect to="/admin/admin-home" />
        ) : null}
      </div>
    );
  }
}

export default Feedback;
