import React, { Component } from "react";
class Logout extends Component {
  state = {};
  render() {
    return (
      <div className="logout-div">
        <span onClick={this.props.handleLogout} className="logout">
          <span className="logout-text">Logout</span>
          <i class="fas fa-sign-out-alt logout-icon" />
        </span>
      </div>
    );
  }
}

export default Logout;
