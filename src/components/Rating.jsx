import React, { Component } from "react";
import StarRatingComponent from "react-star-rating-component";
class Rating extends Component {
  state = {};
  render() {
    return this.props.skillsToBeRated.map((skill, index) => {
      return (
        <div className="form-group row">
          <span className="col-sm-2 " />
          <div className="col-sm-3 skill-label">
            <label>{skill.label}</label>
          </div>
          <span className="col-sm-2 " />
          <div className="col-sm-3  ">
            <StarRatingComponent
              name={skill.label}
              starCount={5}
              onStarClick={this.props.onStarClick}
              className="star"
              value={this.props.ratings[index].rating}
            />
            <span className="col-sm-2 " />
          </div>
        </div>
      );
    });
  }
}

export default Rating;
