import React, { Component } from "react";
import Navbar from "./Navbar";
import { Router, Route, Link } from "react-router-dom";
import Feedback from "./Feedback";
import UserHome from "./UserHome";
import Logout from "./Logout";
import InterviewRequests from "./InterviewRequests";
class Interviewer extends Component {
  state = {};
  componentDidMount() {}
  render() {
    return (
      <div>
        <Route
          path="/interviewer/user-home"
          render={() => {
            return (
              <div>
                <div className="jumbotron home-jumbo">
                  <h1>Welcome {this.props.name}</h1>
                </div>
                <Logout handleLogout={this.props.handleLogout} />
                <div className="form-group row ">
                  <span className="col-sm-2 " style={{ marginLeft: "15px" }}>
                    <Link to={"/interviewer/requests"}>
                      <input
                        type="submit"
                        className="submit-feedback"
                        value="Interview Requests"
                      />
                    </Link>{" "}
                  </span>{" "}
                  <span className="col-sm-2 " style={{ marginLeft: "15px" }}>
                    <Link to={"/interviewer/feedback"}>
                      <input
                        type="submit"
                        className="submit-feedback"
                        value="Give Feedback"
                      />
                    </Link>{" "}
                  </span>{" "}
                </div>

                <UserHome
                  interviewerName={this.props.name}
                  interviewerId={this.props.id}
                  interviewerEmail={this.props.email}
                  token={this.props.token}
                  handleNotification={msg => this.props.handleNotification(msg)}
                  clearStorage={() => this.clearStorage()}
                />
              </div>
            );
          }}
        />
        <Route
          path="/interviewer/feedback"
          render={props => {
            console.log("test", this.props);
            return (
              <div>
                <Logout handleLogout={this.props.handleLogout} />
                <div className="container">
                  <span
                    className="col-sm-2 "
                    style={{ padding: "0", marginTop: "10px" }}
                  >
                    <Link to={"/interviewer/user-home"}>
                      <input
                        type="submit"
                        className="submit-feedback"
                        value="Home"
                      />
                    </Link>{" "}
                  </span>
                </div>
                <Feedback
                  oldInterviewId={
                    props.location.state
                      ? props.location.state.oldInterviewId
                      : null
                  }
                  requestId={
                    props.location.state ? props.location.state.requestId : null
                  }
                  interviewerName={this.props.name}
                  interviewerId={this.props.id}
                  interviewerEmail={this.props.email}
                  token={this.props.token}
                  handleNotification={msg => this.props.handleNotification(msg)}
                  type="interviewer"
                />
              </div>
            );
          }}
        />
        <Route
          path="/interviewer/requests"
          render={() => {
            return (
              <div>
                <div className="jumbotron home-jumbo">
                  <h1>Welcome {this.props.name}</h1>
                </div>
                <Logout handleLogout={this.props.handleLogout} />
                <div className="form-group row ">
                  <span className="col-sm-2 " style={{ marginLeft: "15px" }}>
                    <Link to={"/interviewer/user-home"}>
                      <input
                        type="submit"
                        className="submit-feedback"
                        value="View Feedbacks"
                      />
                    </Link>{" "}
                  </span>{" "}
                  <span className="col-sm-2 " style={{ marginLeft: "15px" }}>
                    <Link to={"/interviewer/feedback"}>
                      <input
                        type="submit"
                        className="submit-feedback"
                        value="Give Feedback"
                      />
                    </Link>{" "}
                  </span>{" "}
                </div>
                <div className="container">
                  <InterviewRequests
                    token={this.props.token}
                    id={this.props.id}
                    handleNotification={msg =>
                      this.props.handleNotification(msg)
                    }
                    type="interviewer"
                  />
                </div>
              </div>
            );
          }}
        />
      </div>
    );
  }
}

export default Interviewer;
