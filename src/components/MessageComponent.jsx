import React, { Component } from "react";
import "react-notifications/lib/notifications.css";
import {
  NotificationContainer,
  NotificationManager
} from "react-notifications";
class MessageComponent extends Component {
  createNotification = type => {
    switch (type) {
      case "login error":
        NotificationManager.error("Server down");
        break;
      case "logged out":
        NotificationManager.success("Successfully logged out");
        break;
      case "Email doesn't exist":
        NotificationManager.error("Email doesn't exist");
        break;
      case "Wrong Password":
        NotificationManager.error("Wrong password");
        break;
      case "Pending":
        NotificationManager.info("Request pending with admin");
        break;
      case "rejected":
        NotificationManager.success("Request rejected");
        break;
      case "approved":
        NotificationManager.success("Request approved");
        break;
      case "removed":
        NotificationManager.success("User removed");
        break;
      case "Feedback saved":
        NotificationManager.success("Feedback Saved");
        break;
      case "signup successful":
        NotificationManager.success(
          "Request sent to the approver",
          "Signup successful"
        );
        break;
      case "Feedback deleted":
        NotificationManager.success("Feedback deleted");
        break;
      case "added":
        NotificationManager.success("Added successfully");
        break;
      case "exists":
        NotificationManager.error("Already exists");
        break;
      case "Deleted Successfully":
        NotificationManager.success("Successfully deleted");
        break;
      case "feedback request sent":
        NotificationManager.success("Interviewer assigned successfully");
        break;
      case "Status changed successfully":
        NotificationManager.success("Status changed successfully");
        break;
      case "password changed":
        NotificationManager.success("Password changed successfully");
        break;
    }
  };

  render() {
    this.createNotification(this.props.message);
    return <NotificationContainer />;
  }
}

export default MessageComponent;
