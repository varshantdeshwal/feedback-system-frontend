import React, { Component } from "react";
class CardDetails extends Component {
  state = {};
  static getDerivedStateFromProps(props, state) {
    return {
      data: props.data
    };
  }
  render() {
    return (
      <div>
        <div className="col-sm-2 feedback">
          <div className="row">
            <span className="col-sm-9 card-name">
              {this.state.data.applicantName}
            </span>
            <span className=" delete" onClick={this.props.confirm}>
              <i class="far fa-trash-alt eye-in" />
            </span>
            <span className=" eye" onClick={this.props.viewInterview}>
              <i class="fa fa-eye fa-1x eye-in" aria-hidden="true" />
            </span>
          </div>
          <div className="row status-date">
            <span className="col-sm-8 card-status">
              {this.state.data.status}
            </span>

            <span className="col-sm-4 card-date">
              {this.state.data.date
                .substring(0, 10)
                .split("-")
                .reverse()
                .join("-")}
            </span>
          </div>
        </div>
      </div>
    );
  }
}

export default CardDetails;
