import React, { Component } from "react";
import DatePicker from "react-datepicker";

import moment from "moment";
import Skills from "./Skills";
import Rating from "./Rating";
class FeedbackFields extends Component {
  state = {};
  handleDateChangeRaw = e => {
    e.preventDefault();
  };
  render() {
    return (
      <div className=" col-lg-8 col-lg-offset-2 border feedback-form-fields">
        <form onSubmit={this.props.submitFeedback}>
          <div className="form-group row row-vertical-space">
            <div className="col-sm-3 feedback-field-label">
              {" "}
              <label>Applicant Name:</label>
            </div>
            <div className="col-sm-3  text-field-padding ">
              {this.props.oldFeedback != "" ? (
                <label className="old-details">
                  {this.props.oldFeedback.applicantName}
                </label>
              ) : (
                <input
                  type="text"
                  className="form-control feedback-field"
                  placeholder="Enter applicant's name"
                  required
                  onChange={e => this.props.handleChange(e, "applicantName")}
                />
              )}
            </div>
            <span className="col-sm-1 " />
            <div className="col-sm-2 feedback-field-label">
              <label>Date:</label>
            </div>
            <div className="col-sm-3  text-field-padding">
              <DatePicker
                className="form-control feedback-field"
                selected={this.props.date}
                placeholderText="Click to select date"
                dateFormat="DD/MM/YYYY"
                maxDate={moment()}
                onChangeRaw={this.handleDateChangeRaw}
                onChange={this.props.handleDate}
                required
              />
            </div>
          </div>
          <div className="form-group row row-vertical-space">
            <div className="col-sm-3 feedback-field-label">
              {" "}
              <label>Work Experience:</label>
            </div>
            <div className="col-sm-1 text-field-padding">
              {this.props.oldFeedback != "" ? (
                <label className="old-details">
                  {this.props.oldFeedback.workExperience.years} Y
                </label>
              ) : (
                <select
                  onChange={e => this.props.handleChange(e, "years")}
                  className="form-control feedback-field"
                >
                  <option disabled value selected>
                    Y
                  </option>

                  {[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15].map(
                    data => {
                      return <option>{data}</option>;
                    }
                  )}
                </select>
              )}
            </div>
            <span className="col-sm-1 " />
            <div className="col-sm-1  text-field-padding">
              {this.props.oldFeedback != "" ? (
                <label className="old-details">
                  {this.props.oldFeedback.workExperience.months} M
                </label>
              ) : (
                <select
                  onChange={e => this.props.handleChange(e, "months")}
                  className="form-control  feedback-field"
                >
                  <option disabled value selected>
                    M
                  </option>

                  {[...Array(12).keys()].map(i => {
                    return <option>{i} </option>;
                  })}
                </select>
              )}
            </div>
            <span className="col-sm-1 " />
            <div className="col-sm-2 feedback-field-label">
              <label>Applying for:</label>
            </div>
            <div className="col-sm-3 text-field-padding">
              {this.props.oldFeedback != "" ? (
                <label className="old-details">
                  {this.props.oldFeedback.applyingFor}
                </label>
              ) : (
                <select
                  onChange={e => this.props.handleChange(e, "applyingFor")}
                  className="form-control drop feedback-field"
                >
                  <option disabled value selected>
                    {" "}
                    Select an option{" "}
                  </option>

                  {this.props.positions.map(data => {
                    return <option>{data} </option>;
                  })}
                </select>
              )}
            </div>
          </div>
          {/* <div className="form-group row row-vertical-space">
            <div className="col-sm-3 feedback-field-label">
              {" "}
              <label>Previous Employer</label>
            </div>
            <div className="col-sm-3  text-field-padding">
              <input
                type="text"
                className="form-control feedback-field"
                placeholder="Enter employer's name"
                required
                onChange={e => this.props.handleChange(e, "previousEmployee")}
              />
            </div>
            <span className="col-sm-1 " />
          </div> */}
          <div className="form-group row row-vertical-space">
            <div className="col-sm-12  ">
              <Skills
                captureSkills={this.props.captureSkills}
                availableSkills={this.props.availableSkills}
              />
            </div>
          </div>
          <div className="form-group row">
            <div className="col-sm-12  ">
              <Rating
                onStarClick={this.props.onStarClick}
                skillsToBeRated={this.props.skillsToBeRated}
                ratings={this.props.ratings}
              />
            </div>
          </div>
          <div className="form-group row row-vertical-space">
            <div className="col-sm-8 text-field-padding ">
              <div class="form-group">
                <label for="comment" className="feedback-field-label">
                  Comments:
                </label>
                <textarea
                  class="form-control comment-section"
                  rows="5"
                  id="comment"
                  onChange={e => this.props.handleChange(e, "comments")}
                  required
                />
              </div>
            </div>
          </div>
          <div className="form-group row row-vertical-space">
            <div className="col-sm-2 text-field-padding border">
              {" "}
              <label for="comment" className="feedback-field-label status">
                Status:
              </label>
            </div>
            <div className="col-sm-10 text-field-padding border">
              <div class="form-group">
                <label className="radio-inline status-label">
                  <input
                    type="radio"
                    name="status"
                    value="Rejected"
                    checked={this.props.status === "Rejected"}
                    onChange={e => this.props.handleChange(e, "status")}
                    class="radio-border"
                  />
                  Rejected
                </label>
                {this.props.oldFeedback.status !=
                "Shortlisted for second round" ? (
                  <label className="radio-inline status-label">
                    <input
                      type="radio"
                      name="status"
                      value="Shortlisted for second round"
                      checked={
                        this.props.status === "Shortlisted for second round"
                      }
                      onChange={e => this.props.handleChange(e, "status")}
                      class="radio-border"
                    />
                    Shortlisted for second round
                  </label>
                ) : null}

                <label className="radio-inline status-label">
                  <input
                    type="radio"
                    name="status"
                    value="Selected"
                    checked={this.props.status === "Selected"}
                    onChange={e => this.props.handleChange(e, "status")}
                    class="radio-border"
                  />
                  Selected
                </label>
                <label className="radio-inline status-label">
                  <input
                    type="radio"
                    name="status"
                    value="On hold"
                    checked={this.props.status === "On hold"}
                    onChange={e => this.props.handleChange(e, "status")}
                    class="radio-border"
                  />
                  On hold
                </label>
              </div>
            </div>
          </div>
          <div className="form-group row">
            <div className="col-sm-2 feedback-field-label">
              {" "}
              <label>Resume:</label>
            </div>

            <div className="col-sm-3 choose-file text-field-padding ">
              {this.props.oldFeedback != "" ? (
                <a
                  rel="noopener noreferrer"
                  href={
                    "https://protected-brook-37812.herokuapp.com/feedbacks/resume/" +
                    this.props.oldFeedback._id +
                    ".pdf"
                  }
                  target="_blank"
                >
                  <i class="fas fa-download fa-2x" />
                </a>
              ) : (
                <input
                  type="file"
                  onChange={this.props.handleFile}
                  className="control-file "
                />
              )}
            </div>
          </div>

          <div className="form-group row ">
            <span className="col-sm-4 " />
            <div className="col-sm-4 text-field-padding ">
              <input
                type="submit"
                className="submit-feedback"
                value="Submit Feedback"
              />
            </div>
            <span className="col-sm-4 " />
          </div>
        </form>
      </div>
    );
  }
}

export default FeedbackFields;
