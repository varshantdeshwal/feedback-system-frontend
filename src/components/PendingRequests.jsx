import React, { Component } from "react";
import getPendingRequests from "../api-calls/user/getPendingRequests";
import declineRequest from "../api-calls/user/declineRequest";
import approveRequest from "../api-calls/user/approveRequest";
import addApprover from "../api-calls/approver/addApprover";
import Spinner from "./Spinner";
class PendingRequests extends Component {
  state = {
    pendingList: [],
    spinner: true,
    showUserRequests: true,
    showInterviewRequests: false
  };
  componentDidMount() {
    getPendingRequests(this.props.approverId, this.props.token).then(res => {
      console.log(res);
      this.setState({ pendingList: res, spinner: false });
    });
  }

  decline = (id, index) => {
    this.setState({ spinner: true });
    declineRequest(id, this.props.token).then(res => {
      console.log(res);
      if (res.message === "declined") {
        let pendingListCopy = this.state.pendingList;
        pendingListCopy.splice(index);
        this.setState({ pendingList: pendingListCopy, spinner: false });
        this.props.handleNotification("rejected");
      }
    });
  };
  approve = (id, index) => {
    this.setState({ spinner: true });
    approveRequest(id, this.props.token).then(res => {
      console.log(res);
      if (res.message === "approved") {
        if (this.state.pendingList[index].requestType === "admin") {
          let data = {
            name: this.state.pendingList[index].name,
            email: this.state.pendingList[index].email,
            approverId: this.state.pendingList[index].approverId
          };
          addApprover(JSON.stringify(data), this.props.token).then(res =>
            console.log(res)
          );
        }
        let pendingListCopy = this.state.pendingList;
        pendingListCopy.splice(index);
        this.setState({ pendingList: pendingListCopy, spinner: false });
        this.props.handleNotification("approved");
      }
    });
  };
  render() {
    return (
      <div>
        {this.state.spinner ? (
          <Spinner />
        ) : (
          <div className=" active-list col-sm-12 col-lg-offset-1">
            {this.state.pendingList.length > 0 ? (
              <div>
                <thead className=" col-sm-12 interviewer-row">
                  <th className="col-sm-3">
                    <label>Name</label>
                  </th>

                  <th className="col-sm-4">
                    <label>Email</label>
                  </th>

                  <th className="col-sm-2">
                    <label>Role</label>
                  </th>

                  <th className="col-sm-3">
                    <label>Actions</label>
                  </th>
                </thead>
                {this.state.pendingList.map((i, index) => (
                  <tr className=" col-sm-12 interviewer-row2">
                    <td className="col-sm-3">{i.name}</td>

                    <td className="col-sm-4">{i.email}</td>

                    <td className="col-sm-2">{i.requestType}</td>

                    <td className="col-sm-3" style={{ paddingLeft: "0" }}>
                      <input
                        type="button"
                        value="Accept"
                        onClick={() => this.approve(i._id, index)}
                        className="accept-button"
                      />

                      <input
                        type="button"
                        value="Reject"
                        onClick={() => this.decline(i._id, index)}
                        className="reject-button"
                      />
                    </td>
                  </tr>
                ))}
              </div>
            ) : (
              <h3 className="empty">No pending requests</h3>
            )}
          </div>
        )}
      </div>
    );
  }
}

export default PendingRequests;
