import React, { Component } from "react";
import Select from "react-select";
import getActiveInterviewers from "../api-calls/user/getActiveInterviewers";
class Filter extends Component {
  state = { interviewers: [] };
  componentDidMount() {
    // getActiveInterviewers(
    //   this.props.approverId,
    //   this.props.id,
    //   this.props.token
    // ).then(res => {
    //   console.log(res);
    //   if (res.message !== "auth failed") {
    //     let options = [];
    //     res.map(data => {
    //       options.push({ label: data.name, value: data._id });
    //     });
    //     options.unshift({ label: "Me", value: this.props.id });
    //     this.setState({ interviewers: options });
    //   }
    // });
  }
  render() {
    return (
      <div>
        <Select
          closeMenuOnSelect={false}
          onChange={this.props.captureFilter}
          options={this.props.options}
          isMulti="true"
          isSearchable="true"
          placeholder="Search and filter feedbacks..."
          className="multi-select"
          value={this.props.selected}
        />
      </div>
    );
  }
}

export default Filter;
