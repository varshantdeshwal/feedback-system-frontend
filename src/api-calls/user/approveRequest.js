import url from "../url";

var result = async function approveRequest(id, token) {
  const res = await fetch(
    url + "users/" + id,

    {
      method: "PUT",
      headers: {
        Authorization: "Bearer " + token
      }
    }
  );

  const data = await res.json();

  return data;
};

export default result;
