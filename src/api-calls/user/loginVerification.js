import url from "../url";

var result = async function addInterviewer(data) {
  const res = await fetch(
    url + "users/login",

    {
      method: "POST",
      headers: {
        Accept: "application/json, text/plain, */*",
        "Content-Type": "application/json"
      },
      body: data
    }
  );
  const response = await res.json();
  return response;
};

export default result;
