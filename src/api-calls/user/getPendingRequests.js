import url from "../url";

var result = async function getPendingRequests(id, token) {
  const res = await fetch(
    url + "users/pending/" + id,

    {
      method: "GET",
      headers: {
        Authorization: "Bearer " + token
      }
    }
  );

  const data = await res.json();

  return data;
};

export default result;
