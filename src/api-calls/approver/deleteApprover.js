import url from "../url";
var result = async function deleteUser(email, token) {
  const res = await fetch(
    url + "admins/" + email,

    {
      method: "DELETE",
      headers: {
        Authorization: "Bearer " + token
      }
    }
  );

  const data = await res.json();

  return data;
};

export default result;
