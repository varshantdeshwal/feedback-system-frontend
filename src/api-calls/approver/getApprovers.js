import url from "../url";
var result = async function getApprovers() {
  const res = await fetch(
    url + "admins/",

    {
      method: "GET"
    }
  );

  const data = await res.json();

  return data;
};

export default result;
