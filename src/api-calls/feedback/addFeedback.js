import url from "../url";
var result = async function addFeedback(feedback, token) {
  const res = await fetch(
    url + "feedbacks",

    {
      method: "POST",
      headers: {
        Accept: "application/json, text/plain, */*",
        "Content-Type": "application/json",
        Authorization: "Bearer " + token
      },
      body: feedback
    }
  );
  const response = await res.json();
  return response;
};

export default result;
