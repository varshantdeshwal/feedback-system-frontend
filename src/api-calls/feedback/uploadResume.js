import url from "../url";

var result = async function uploadResume(formData, token) {
  const res = await fetch(
    url + "feedbacks/uploads",

    {
      method: "POST",
      headers: {
        Authorization: "Bearer " + token
      },
      body: formData
    }
  );
  const response = await res.json();
  return response;
};

export default result;
