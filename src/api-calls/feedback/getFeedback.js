import url from "../url";

var result = async function getFeedback(id, token) {
  const res = await fetch(
    url + "feedbacks/feedback/" + id,

    {
      method: "GET",
      headers: {
        Authorization: "Bearer " + token
      }
    }
  );

  const data = await res.json();

  return data;
};

export default result;
