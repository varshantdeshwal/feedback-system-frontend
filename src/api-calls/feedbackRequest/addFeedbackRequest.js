import url from "../url";

var result = async function addFeedbackRequest(request, token) {
  const res = await fetch(
    url + "feedback-requests",

    {
      method: "POST",
      headers: {
        Accept: "application/json, text/plain, */*",
        "Content-Type": "application/json",
        Authorization: "Bearer " + token
      },
      body: request
    }
  );
  const response = await res.json();
  return response;
};

export default result;
