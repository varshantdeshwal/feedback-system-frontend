import url from "../url";

var result = async function deleteFeedbackRequest(id, token) {
  const res = await fetch(
    url + "feedback-requests/" + id,

    {
      method: "DELETE",
      headers: {
        Authorization: "Bearer " + token
      }
    }
  );

  const data = await res.json();

  return data;
};

export default result;
