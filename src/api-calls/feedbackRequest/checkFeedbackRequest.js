import url from "../url";

var result = async function checkFeedbackRequest(id, token) {
  const res = await fetch(
    url + "feedback-requests/check/" + id,

    {
      method: "GET",
      headers: {
        Authorization: "Bearer " + token
      }
    }
  );

  const data = await res.json();

  return data;
};

export default result;
