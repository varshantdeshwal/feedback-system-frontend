import url from "../url";

var result = async function get(collection, token) {
  const res = await fetch(
    url + collection,

    {
      method: "GET",
      headers: {
        Authorization: "Bearer " + token
      }
    }
  );

  const data = await res.json();

  return data;
};

export default result;
