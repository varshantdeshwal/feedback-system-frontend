import url from "../url";
var result = async function add(data, token, collection) {
  const res = await fetch(
    url + collection + "/",

    {
      method: "POST",
      headers: {
        Accept: "application/json, text/plain, */*",
        "Content-Type": "application/json",
        Authorization: "Bearer " + token
      },
      body: data
    }
  );
  const response = await res.json();
  return response;
};

export default result;
