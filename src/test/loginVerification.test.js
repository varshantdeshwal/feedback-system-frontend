import loginVerification from "../calls/user/loginVerification";
test("login with email that doesn't exist", () => {
  expect.assertions(1);
  return loginVerification(
    JSON.stringify({ email: "admin1@pqr", password: "ad1" })
  ).then(data => {
    expect(data).toEqual({ message: "email doesn't exist" });
  });
});

test("login with wrong password", () => {
  expect.assertions(1);
  return loginVerification(
    JSON.stringify({ email: "admin1@abc", password: "ad2" })
  ).then(data => {
    expect(data).toEqual({ message: "wrong password" });
  });
});

test("login when request is pending with admin", () => {
  expect.assertions(1);
  return loginVerification(
    JSON.stringify({ email: "varshant@xyz", password: "varshant" })
  ).then(data => {
    expect(data.message).toEqual("pending");
  });
});

test("login with correct email and password", () => {
  expect.assertions(1);
  return loginVerification(
    JSON.stringify({ email: "admin1@abc", password: "ad1" })
  ).then(data => {
    expect(data.message).toEqual("auth successful");
  });
});
