import getFeedbacks from "../calls/feedback/getFeedbacks";

test("get feedbacks with wrong/no token", () => {
  return getFeedbacks("5c010f1f2af7891b90825097", "").then(data => {
    expect(data.message).toEqual("auth failed");
  });
});
test("get feedbacks with wrong/no token", () => {
  return getFeedbacks(
    "5c010f1f2af7891b90825097",
    "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImFkbWluMUBhYmMiLCJ1c2VySWQiOiI1YzAxMGYxZjJhZjc4OTFiOTA4MjUwOTciLCJuYW1lIjoiYWRtaW4gMSIsInVzZXJUeXBlIjoiYWRtaW4iLCJpYXQiOjE1NDQ0MTc0OTYsImV4cCI6MTU0NDQ1MzQ5Nn0.ak9h2G09463PsSGjh8_d5Ec_5NRiPc4kOIOquof_MhU"
  ).then(data => {
    expect(data).toBeDefined();
  });
});
